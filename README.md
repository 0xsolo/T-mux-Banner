# T-mux-Banner

customize your Termux terminal like a pro 

# Installation

$ apt update

$ apt upgrade

$ apt install git

$ git clone https://gitlab.com/0xsolo/t-mux-banner.git

$ cd T-mux-Banner

$ bash install

$ bash TXB.sh


# Screenshot

![20200628_143242](/uploads/2d274b02a1eea3954fd7a058ccf132f4/20200628_143242.jpg)


![20200628_143144](/uploads/6d2055509326e75dd46966d25f968ce8/20200628_143144.jpg)


![20200628_143306](/uploads/be1a125e48df0faab33251d289174fb4/20200628_143306.jpg)


![20200628_143504](/uploads/165cd1a1d12bd67f087887001bfd85ca/20200628_143504.jpg)


![20200628_143441](/uploads/45599eb7a303255e4f75a2d7bcd3f5ac/20200628_143441.jpg)


![20200628_143410](/uploads/42d191b76d725898d5eac7967d605d0d/20200628_143410.jpg)


![20200628_143539](/uploads/0dd1245588ff2ee39158b17f769895aa/20200628_143539.jpg)


#install and use more banners. 
